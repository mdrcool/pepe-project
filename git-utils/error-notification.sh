BODY="{
  \"title\": \"Fix the bug; commit: ${CI_COMMIT_SHA}\",
  \"labels\": \"bug\",
  \"assignee_ids\": [ ${GITLAB_USER_ID} ],
  \"description\": \"Branch: ${CI_COMMIT_REF_NAME}, commit, that arises the bug: ${CI_COMMIT_SHA}\"
}";

curl -X POST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/issues" \
  --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";

BODY_SLACK="{
  \"text\": \"New bug from User #${GITLAB_USER_ID}! Come and fix it!\"
}"

curl -X POST https://hooks.slack.com/services/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 \
  --header 'Content-type: application/json' \
  --data "${BODY_SLACK}" 