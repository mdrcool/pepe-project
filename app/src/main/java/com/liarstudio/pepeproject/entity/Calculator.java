package com.liarstudio.pepeproject.entity;

public class Calculator {
    private int x;
    private  int y;

    public void setX(String x) {
        this.x = setValue(x);
    }

    public void setY(String y) {
        this.y = setValue(y);
    }

    private int setValue(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException|NullPointerException nfe) {
            return 0;
        }
    }

    public Integer add() {
        return x + y;
    }
}
