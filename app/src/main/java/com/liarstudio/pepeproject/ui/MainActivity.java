package com.liarstudio.pepeproject.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.liarstudio.pepeproject.entity.Calculator;
import com.liarstudio.pepeproject.R;

public class MainActivity extends AppCompatActivity {

    EditText xEt;
    EditText yEt;
    TextView resultTv;
    Button countBtn;

    Calculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calculator = new Calculator();
        findViews();
        initListeners();
    }

    void findViews() {
        countBtn = findViewById(R.id.button_count);
        xEt = findViewById(R.id.x_et);
        yEt = findViewById(R.id.y_et);
        resultTv = findViewById(R.id.result_tv);
    }

    private void initListeners() {
        countBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculator.setX(xEt.getText().toString());
                calculator.setY(yEt.getText().toString());
                resultTv.setText(calculator.add().toString());
            }
            asdasdasdads1
        });
    }
 }