package com.liarstudio.pepeproject;

import com.liarstudio.pepeproject.entity.Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CalculatorUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {

        String x = "5";
        String y = "9";
        int expected = 14;
        int actual;
        Calculator calculator = new Calculator();

        calculator.setX(x);
        calculator.setY(y);
        actual = calculator.add();

        assertEquals(expected, actual);
    }

    @Test
    public void numberFormatException_handlingCorrect() {
        String x = "not a number";
        String y = "9";
        int expected = 9;
        int actual;
        Calculator calculator = new Calculator();

        calculator.setX(x); calculator.setY(y);
        actual = calculator.add();

        assertEquals(expected, actual);
    }

    @Test
    public void nullPointerException_handlingCorrect() {
        String x = null;
        String y = null;
        int expected = 0;
        int actual;
        Calculator calculator = new Calculator();

        calculator.setX(x); calculator.setY(y);
        actual = calculator.add();

        assertEquals(expected, actual);
    }
}